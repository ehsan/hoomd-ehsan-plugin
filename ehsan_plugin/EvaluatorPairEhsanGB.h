// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: jglaser

#ifndef __EVALUATOR_EHSAN_PAIR_GB_H__
#define __EVALUATOR_EHSAN_PAIR_GB_H__

#ifndef NVCC
#include <string>
#endif

#define HOOMD_GB_MIN(i,j) ((i > j) ? j : i)
#define HOOMD_GB_MAX(i,j) ((i > j) ? i : j)

#include "hoomd/VectorMath.h"
#include "hoomd/HOOMDMath.h"

#include "hoomd/RandomNumbers.h"
#include "hoomd/RNGIdentifiers.h"

/*! \file EvaluatorPairGB.h
    \brief Defines a an evaluator class for the Gay-Berne potential
*/

// need to declare these class methods with __device__ qualifiers when building in nvcc
//! HOSTDEVICE is __host__ __device__ when included in nvcc and blank when included into the host compiler
#ifdef NVCC
#define HOSTDEVICE __host__ __device__
#else
#define HOSTDEVICE
#endif

#define PI 3.14159265

struct pair_ehsangb_params
    {
    Scalar epsilon;   //! The energy scale.
    Scalar lperp;     //! The semiaxis length perpendicular to the particle orientation.
    Scalar lpar;      //! The semiaxis length parallel to the particle orientation.

    //! Load dynamic data members into shared memory and increase pointer
    /*! \param ptr Pointer to load data to (will be incremented)
        \param available_bytes Size of remaining shared memory allocation
     */
    HOSTDEVICE void load_shared(char *& ptr, unsigned int &available_bytes) const
        {
        // No-op for this struct since it contains no arrays.
        }
    };


// Nullary structure required by AnisoPotentialPair.
struct gb_shape_params
    {
    HOSTDEVICE gb_shape_params() {}

    //! Load dynamic data members into shared memory and increase pointer
    /*! \param ptr Pointer to load data to (will be incremented)
        \param available_bytes Size of remaining shared memory allocation
     */
    HOSTDEVICE void load_shared(char *& ptr, unsigned int &available_bytes) const {}

    #ifdef ENABLE_CUDA
    //! Attach managed memory to CUDA stream
    void attach_to_stream(cudaStream_t stream) const {}
    #endif
    };

/*!
 * Gay-Berne potential as formulated by Allen and Germano,
 * with shape-independent energy parameter, for identical uniaxial particles.
 */

class EvaluatorPairEhsanGB
    {
    public:
        typedef pair_ehsangb_params param_type;
        typedef gb_shape_params shape_param_type;

        //! Constructs the pair potential evaluator
        /*! \param _dr Displacement vector between particle centers of mass
            \param _rcutsq Squared distance at which the potential goes to 0
            \param _q_i Quaternion of i^th particle
            \param _q_j Quaternion of j^th particle
            \param _params Per type pair parameters of this potential
        */
        HOSTDEVICE EvaluatorPairEhsanGB(const Scalar3& _dr,
                               const Scalar4& _qi,
                               const Scalar4& _qj,
                               const Scalar _rcutsq,
                               const param_type& _params)
            : dr(_dr),rcutsq(_rcutsq),qi(_qi),qj(_qj),
              params(_params)
            {
            }

        //! Set i and j, (particle tags), and the timestep
        HOSTDEVICE void set_seed_ij_timestep(unsigned int seed, unsigned int i, unsigned int j, unsigned int timestep)
            {
            m_seed = seed;
            m_i = i;
            m_j = j;
            m_timestep = timestep;
            }
        //! uses diameter
        HOSTDEVICE static bool needsDiameter()
            {
            return false;
            }

        //! Whether the pair potential uses shape.
        HOSTDEVICE static bool needsShape()
            {
            return false;
            }

        //! Whether the pair potential needs particle tags.
        HOSTDEVICE static bool needsTags()
            {
            return false;
            }

        //! whether pair potential requires charges
        HOSTDEVICE static bool needsCharge( )
            {
            return false;
            }

        //! Accept the optional diameter values
        /*! \param di Diameter of particle i
            \param dj Diameter of particle j
        */
        HOSTDEVICE void setDiameter(Scalar d_i, Scalar d_j)
	    {
	    }

        //! Accept the optional shape values
        /*! \param shape_i Shape of particle i
            \param shape_j Shape of particle j
        */
        HOSTDEVICE void setShape(const shape_param_type *shapei, const shape_param_type *shapej) {}

        //! Accept the optional tags
        /*! \param tag_i Tag of particle i
            \param tag_j Tag of particle j
        */
        HOSTDEVICE void setTags(unsigned int tagi, unsigned int tagj) {}

        //! Accept the optional charge values
        /*! \param qi Charge of particle i
            \param qj Charge of particle j
        */
        HOSTDEVICE void setCharge(Scalar qi, Scalar qj)
        {
	}

        //! Evaluate the force and energy
        /*! \param force Output parameter to write the computed force.
            \param pair_eng Output parameter to write the computed pair energy.
            \param energy_shift If true, the potential must be shifted so that V(r) is continuous at the cutoff.
            \param torque_i The torque exerted on the i^th particle.
            \param torque_j The torque exerted on the j^th particle.
            \return True if they are evaluated or false if they are not because we are beyond the cutoff.
        */
        HOSTDEVICE  bool
        evaluate(Scalar3& force, Scalar& pair_eng, bool energy_shift, Scalar3& torque_i, Scalar3& torque_j)
            {
            Scalar rsq = dot(dr,dr);
            Scalar r = fast::sqrt(rsq);
            vec3<Scalar> unitr = fast::rsqrt(dot(dr,dr))*dr;

            // obtain rotation matrices (space->body)
            rotmat3<Scalar> rotA(conj(qi));
            rotmat3<Scalar> rotB(conj(qj));

            // last row of rotation matrix
            vec3<Scalar> a3 = rotA.row2;
            vec3<Scalar> b3 = rotB.row2;

            Scalar ca = dot(a3,unitr);
            Scalar cb = dot(b3,unitr);
            Scalar cab = dot(a3,b3);

            Scalar rcut = fast::sqrt(rcutsq);
            Scalar dUdphi,dUdr;

	    // Elham:
            Scalar mu = params.epsilon;
            // compute the force divided by r in force_divr
	    // Ehsan:
	    dUdr  = 0.0; //-Scalar(24.0)*params.epsilon*(zeta6inv/zeta*(Scalar(2.0)*zeta6inv-Scalar(1.0)))/sigma_min;
	    dUdphi = 0.0; //dUdr*Scalar(1.0/2.0)*sigma*sigma*sigma;

	    // Elham:
	    //pair_eng = Scalar(4.0)*params.epsilon*zeta6inv * (zeta6inv - Scalar(1.0));
	    pair_eng = mu*(4*qi.v.z*qi.s*qj.v.z*qj.s + (Scalar(2.0)*qi.s*qi.s-1)*(Scalar(2)*qj.s*qj.s-1));

            // compute vector force and torque
            //Scalar r2inv = Scalar(1.0)/rsq;
            //vec3<Scalar> fK = -r2inv*dUdphi*kappa;
            //vec3<Scalar> f = -dUdr*unitr + fK + r2inv*dUdphi*unitr*dot(kappa,unitr);
            // force = vec_to_scalar3(f);
            // Elham:
            force = force*Scalar(0.0);


	    /*	    unsigned int m_oi, m_oj;
	    // initialize the RNG
	    if (m_i > m_j)
		{
		m_oi = m_j;
		m_oj = m_i;
		}
	    else
		{
		m_oi = m_i;
		m_oj = m_j;
		}
	    hoomd::RandomGenerator rng(hoomd::RNGIdentifier::EvaluatorPairDPDThermo, m_seed, m_oi, m_oj, m_timestep);
*/	    
	    // Generate a single random number
	    Scalar alpha = Scalar(0.0);
	    Scalar beta = Scalar(0.0); //hoomd::UniformDistribution<Scalar>(0,1)(rng);
	    Scalar zi = Scalar(0.0);
	    Scalar zj = Scalar(0.0);

	    if ((r <= rcut)) // do tumble!
	        {
		alpha = Scalar(1.0); //hoomd::UniformDistribution<Scalar>(-1,1)(rng);
		//torque_i.z = mu*alpha;

		zi = (Scalar(2.0)*qj.v.z*qj.s*(Scalar(2.0)*qi.s*qi.s-1) - Scalar(2.0)*qi.v.z*qi.s*(Scalar(2.0)*qj.s*qj.s-1));
		zj = (Scalar(2.0)*qi.v.z*qi.s*(Scalar(2.0)*qj.s*qj.s-1) - Scalar(2.0)*qj.v.z*qj.s*(Scalar(2.0)*qi.s*qi.s-1));
		
		if (params.lperp<1)
		    {// Elham: 
		    torque_i.z = mu*zi;
		    torque_j.z = mu*zj;
		    }
		else
		    {
		      Scalar ti = 2.0*fast::acos(qi.s);
		      Scalar tj = 2.0*fast::acos(qj.s);
		      //printf("qi.s: %f, qj.s: %f", qi.s, qj.s);
		      //printf("ti: %f, tj: %f", ti, tj);
		      ti = fmod((ti-tj)+PI, 2*PI) - PI;
		      tj = -ti;
		      //printf("ti: %f, tj: %f", ti, tj);
		      torque_i.z = mu*ti;
		      torque_j.z = mu*tj;
		    }
	        }
	      
	    // random torque:
	    //torque_j.z = -torque_i.z;
	    
            torque_i.x = torque_i.y = Scalar(0.0);
            torque_j.x = torque_j.y = Scalar(0.0);
            return true;
            }

        #ifndef NVCC
        //! Get the name of the potential
        /*! \returns The potential name. Must be short and all lowercase, as this is the name energies will be logged as
            via analyze.log.
        */
        static std::string getName()
            {
            return "gb";
            }

        std::string getShapeSpec() const
            {
            std::ostringstream shapedef;
            shapedef << "{\"type\": \"Ellipsoid\", \"a\": " << params.lperp <<
                        ", \"b\": " << params.lperp <<
                        ", \"c\": " << params.lpar <<
                        "}";
            return shapedef.str();
            }
        #endif

    protected:
        vec3<Scalar> dr;   //!< Stored dr from the constructor
        Scalar rcutsq;     //!< Stored rcutsq from the constructor
        quat<Scalar> qi;   //!< Orientation quaternion for particle i
        quat<Scalar> qj;   //!< Orientation quaternion for particle j
	// Ehsan:
        unsigned int m_seed; //!< User set seed for thermostat PRNG
        unsigned int m_i;   //!< index of first particle (should it be tag?).  For use in PRNG
        unsigned int m_j;   //!< index of second particle (should it be tag?). For use in PRNG
        unsigned int m_timestep; //!< timestep for use in PRNG
        //
        const param_type &params;  //!< The pair potential parameters
    };


#undef HOOMD_GB_MIN
#undef HOOMD_GB_MAX
#endif // __EVALUATOR_EHSAN_PAIR_GB_H__
