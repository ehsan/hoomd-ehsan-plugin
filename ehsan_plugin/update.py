# Copyright (c) 2009-2019 The Regents of the University of Michigan
# This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

# this simple python interface just activates the c++ ExampleUpdater from cppmodule
# Check out any of the python code in lib/hoomd-python-module/hoomd_script for moreexamples

# First, we need to import the C++ module. It has the same name as this module (ehsan_plugin) but with an underscore
# in front
from hoomd.ehsan_plugin import _ehsan_plugin

# Next, since we are extending an updater, we need to bring in the base class updater and some other parts from
# hoomd_script
import hoomd

## Zeroes all particle velocities
#
# Every \a period time steps, particle velocities are modified so that they are all zero
#
class example(hoomd.update._updater):
    ## Initialize the velocity zeroer
    #
    # \param period Velocities will be zeroed every \a period time steps
    #
    # \b Examples:
    # \code
    # ehsan_plugin.update.example()
    # zeroer = ehsan_plugin.update.example(period=10)
    # \endcode
    #
    # \a period can be a function: see \ref variable_period_docs for details
    def __init__(self, period=1):
        hoomd.util.print_status_line();

        # initialize base class
        hoomd.update._updater.__init__(self);

        # initialize the reflected c++ class
        if not hoomd.context.exec_conf.isCUDAEnabled():
            self.cpp_updater = _ehsan_plugin.ExampleUpdater(hoomd.context.current.system_definition);
        else:
            self.cpp_updater = _ehsan_plugin.ExampleUpdaterGPU(hoomd.context.current.system_definition);

        self.setupUpdater(period);


class box_resize(hoomd.update._updater):
    R""" Rescale the system box size.
    """

    def __init__(self, Lx = None, Ly = None, Lz = None, xy = None, xz = None, yz = None, period = 1, L = None, phase=0, scale_particles=True,
                    constant_shear = False, xy_shear_rate = 0.0, yz_shear_rate = 0.0, xz_shear_rate = 0.0):
        hoomd.util.print_status_line();

        # initialize base class
        hoomd.update._updater.__init__(self);

        self.metadata_fields = ['period']

        if L is not None:
            Lx = L;
            Ly = L;
            Lz = L;

        if Lx is None and Ly is None and Lz is None and xy is None and xz is None and yz is None and (constant_shear==False):
            hoomd.context.msg.warning("update.box_resize: Ignoring request to setup updater without parameters\n")
            return


        box = hoomd.context.current.system_definition.getParticleData().getGlobalBox();
        # setup arguments
        if Lx is None:
            Lx = box.getL().x;
        if Ly is None:
            Ly = box.getL().y;
        if Lz is None:
            Lz = box.getL().z;

        if xy is None:
            xy = box.getTiltFactorXY();
        if xz is None:
            xz = box.getTiltFactorXZ();
        if yz is None:
            yz = box.getTiltFactorYZ();

        Lx = hoomd.variant._setup_variant_input(Lx);
        Ly = hoomd.variant._setup_variant_input(Ly);
        Lz = hoomd.variant._setup_variant_input(Lz);

        xy = hoomd.variant._setup_variant_input(xy);
        xz = hoomd.variant._setup_variant_input(xz);
        yz = hoomd.variant._setup_variant_input(yz);

        # store metadata
        self.Lx = Lx
        self.Ly = Ly
        self.Lz = Lz
        self.xy = xy
        self.xz = xz
        self.yz = yz
        self.metadata_fields = ['Lx','Ly','Lz','xy','xz','yz']

        # create the c++ mirror class
        #self.cpp_updater = _hoomd.EhsanBoxResizeUpdater(hoomd.context.current.system_definition, Lx.cpp_variant, Ly.cpp_variant, Lz.cpp_variant,
        self.cpp_updater = _ehsan_plugin.EhsanBoxResizeUpdater(hoomd.context.current.system_definition, Lx.cpp_variant, Ly.cpp_variant, Lz.cpp_variant,
                                                  xy.cpp_variant, xz.cpp_variant, yz.cpp_variant, constant_shear);
        self.cpp_updater.setParams(scale_particles, constant_shear, xy_shear_rate, yz_shear_rate, xz_shear_rate);

        if period is None:
            self.cpp_updater.update(hoomd.context.current.system.getCurrentTimeStep());
        else:
            self.setupUpdater(period, phase);
