// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: jglaser

/*! \file AllDriverEhsanAnisoPotentialPairGPU.cu
    \brief Defines the driver functions for computing all types of anisotropic pair forces on the GPU
*/

#ifndef __ALL_DRIVER_EHSAN_ANISO_POTENTIAL_PAIR_GPU_CUH__
#define __ALL_DRIVER_EHSAN_ANISO_POTENTIAL_PAIR_GPU_CUH__

#include "hoomd/md/AnisoPotentialPairGPU.cuh"
#include "EvaluatorPairEhsanGB.h"

//! Compute dipole forces and torques on the GPU with EvaluatorPairDipole

cudaError_t gpu_compute_pair_aniso_forces_ehsangb(const a_pair_args_t&,
            const EvaluatorPairEhsanGB::param_type*,
            const EvaluatorPairEhsanGB::shape_param_type*);

#endif
