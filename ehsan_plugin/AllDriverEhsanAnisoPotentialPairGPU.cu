// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: jglaser

#include "AllDriverEhsanAnisoPotentialPairGPU.cuh"

/*! \file AllDriverEhsanAnisoPotentialPairGPU.cu
    \brief Defines the driver functions for computing all types of anisotropic pair forces on the GPU
*/

cudaError_t gpu_compute_pair_aniso_forces_ehsangb(const a_pair_args_t& pair_args,
            const EvaluatorPairEhsanGB::param_type* d_param,
            const EvaluatorPairEhsanGB::shape_param_type* d_shape_param)
    {
    return gpu_compute_pair_aniso_forces<EvaluatorPairEhsanGB>(pair_args, d_param, d_shape_param);
    }

