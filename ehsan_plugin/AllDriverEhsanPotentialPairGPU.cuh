// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander / Everyone is free to add additional potentials

/*! \file AllDriverPotentialPairGPU.cuh
    \brief Declares driver functions for computing all types of pair forces on the GPU
*/

#ifndef __ALL_DRIVER_EHSAN_POTENTIAL_PAIR_GPU_CUH__
#define __ALL_DRIVER_EHSAN_POTENTIAL_PAIR_GPU_CUH__

#include <hoomd/md/PotentialPairGPU.cuh>
//#include "PotentialPairGPU.cuh"

//! Compute lj pair forces on the GPU with PairEvaluatorLJ
cudaError_t gpu_compute_ehsanljtemp_forces(const pair_args_t& pair_args,
                                      const Scalar2 *d_params);


#endif
