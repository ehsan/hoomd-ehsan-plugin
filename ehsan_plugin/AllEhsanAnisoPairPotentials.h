// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: jglaser

#ifndef __ALL_EHSAN_ANISO_PAIR_POTENTIALS__H__
#define __ALL_EHSAN_ANISO_PAIR_POTENTIALS__H__

#include "hoomd/md/AnisoPotentialPair.h"
//#include <hoomd/md/AnisoPotentialPair.h>

#include "EvaluatorPairEhsanGB.h"

#ifdef ENABLE_CUDA
#include "hoomd/md/AnisoPotentialPairGPU.h"
#include "hoomd/md/AnisoPotentialPairGPU.cuh"
#include "AllDriverEhsanAnisoPotentialPairGPU.cuh"
#endif

/*! \file AllAnisoPairPotentials.h
    \brief Handy list of typedefs for all of the templated pair potentials in hoomd
*/

//! Pair potential force compute for Gay-Berne forces and torques
typedef AnisoPotentialPair<EvaluatorPairEhsanGB> AnisoPotentialPairEhsanGB;

#ifdef ENABLE_CUDA
//! Pair potential force compute for Gay-Berne forces and torques on the GPU
typedef AnisoPotentialPairGPU<EvaluatorPairEhsanGB, gpu_compute_pair_aniso_forces_ehsangb> AnisoPotentialPairEhsanGBGPU;
#endif

//

#endif // __ALL_ANISO_PAIR_POTENTIALS_H__
