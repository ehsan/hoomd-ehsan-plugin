# Copyright (c) 2009-2019 The Regents of the University of Michigan
# This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

from hoomd.ehsan_plugin import _ehsan_plugin

from hoomd import _hoomd
from hoomd.md import _md
from hoomd.md.pair import pair
from hoomd.md.pair import ai_pair
from hoomd.md.pair import coeff
from hoomd.md import force
from hoomd.md import nlist as nl  # to avoid naming conflicts
import hoomd
import hoomd.md

import math
import sys
import json
from collections import OrderedDict

# class ehsanlj(pair=hoomd.md.pair.pair):


class ehsanlj(pair):
    def __init__(self, r_cut, nlist, name=None):
        hoomd.util.print_status_line()

        # tell the base class how we operate

        # initialize the base class
        pair.__init__(self, r_cut, nlist, name)

        # create the c++ mirror class
        if not hoomd.context.exec_conf.isCUDAEnabled():
            self.cpp_force = _ehsan_plugin.PotentialPairEhsanLJ(
                hoomd.context.current.system_definition, self.nlist.cpp_nlist, self.name)
            self.cpp_class = _ehsan_plugin.PotentialPairEhsanLJ
        else:
            self.nlist.cpp_nlist.setStorageMode(
                _md.NeighborList.storageMode.full)
            self.cpp_force = _ehsan_plugin.PotentialPairEhsanLJGPU(
                hoomd.context.current.system_definition, self.nlist.cpp_nlist, self.name)
            self.cpp_class = _ehsan_plugin.PotentialPairEhsanLJGPU

        hoomd.context.current.system.addCompute(
            self.cpp_force, self.force_name)

        # setup the coefficient options
        self.required_coeffs = ['epsilon', 'sigma', 'alpha']
        self.pair_coeff.set_default_coeff('alpha', 1.0)

    def process_coeff(self, coeff):
        epsilon = coeff['epsilon']
        sigma = coeff['sigma']
        alpha = coeff['alpha']

        lj1 = 4.0 * epsilon * math.pow(sigma, 12.0)
        lj2 = alpha * 4.0 * epsilon * math.pow(sigma, 6.0)
        return _hoomd.make_scalar2(lj1, lj2)


class gb(ai_pair):
    R""" Gay-Berne anisotropic pair potential.
    Example::

        nl = nlist.cell()
        gb = pair.gb(r_cut=2.5, nlist=nl)
        gb.pair_coeff.set('A', 'A', epsilon=1.0, lperp=0.45, lpar=0.5)
        gb.pair_coeff.set('A', 'B', epsilon=2.0, lperp=0.45, lpar=0.5, r_cut=2**(1.0/6.0));

    """

    def __init__(self, r_cut, nlist, name=None):
        hoomd.util.print_status_line()

        # tell the base class how we operate

        # initialize the base class
        ai_pair.__init__(self, r_cut, nlist, name)

        # create the c++ mirror class
        if not hoomd.context.exec_conf.isCUDAEnabled():
            self.cpp_force = _ehsan_plugin.AnisoPotentialPairEhsanGB(
                hoomd.context.current.system_definition, self.nlist.cpp_nlist, self.name)
            self.cpp_class = _ehsan_plugin.AnisoPotentialPairEhsanGB
        else:
            self.nlist.cpp_nlist.setStorageMode(
                _md.NeighborList.storageMode.full)
            self.cpp_force = _ehsan_plugin.AnisoPotentialPairEhsanGBGPU(
                hoomd.context.current.system_definition, self.nlist.cpp_nlist, self.name)
            self.cpp_class = _ehsan_plugin.AnisoPotentialPairEhsanGBGPU

        hoomd.context.current.system.addCompute(
            self.cpp_force, self.force_name)

        # setup the coefficient options
        self.required_coeffs = ['epsilon', 'lperp', 'lpar']

    def process_coeff(self, coeff):
        epsilon = coeff['epsilon']
        lperp = coeff['lperp']
        lpar = coeff['lpar']

        return _ehsan_plugin.make_pair_ehsangb_params(epsilon, lperp, lpar)

    def get_type_shapes(self):
        """Get all the types of shapes in the current simulation.

        Example:

            >>> my_gb.get_type_shapes()
            [{'type': 'Ellipsoid', 'a': 1.0, 'b': 1.0, 'c': 1.5}]

        Returns:
            A list of dictionaries, one for each particle type in the system.
        """
        return super(ai_pair, self)._return_type_shapes()
