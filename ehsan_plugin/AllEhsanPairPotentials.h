// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.


// Maintainer: joaander / Anyone is free to add their own pair potentials here

#ifndef __EHSAN_PAIR_POTENTIALS__H__
#define __EHSAN_PAIR_POTENTIALS__H__

//#include "PotentialPair.h"
#include <hoomd/md/PotentialPair.h>
#include "EvaluatorPairEhsanLJ.h"

#ifdef ENABLE_CUDA
//#include "PotentialPairGPU.h"
#include <hoomd/md/PotentialPairGPU.h>
#include "AllDriverEhsanPotentialPairGPU.cuh"
#endif

/*! \file AllEhsanPairPotentials.h
    \brief Handy list of typedefs for all of the templated pair potentials in hoomd
*/

#ifdef NVCC
#error This header cannot be compiled by nvcc
#endif

//! Pair potential force compute for lj forces
typedef PotentialPair<EvaluatorPairEhsanLJ> PotentialPairEhsanLJ;

#ifdef ENABLE_CUDA
//! Pair potential force compute for lj forces on the GPU
typedef PotentialPairGPU< EvaluatorPairEhsanLJ, gpu_compute_ehsanljtemp_forces > PotentialPairEhsanLJGPU;
#endif

#endif // __EHSAN_PAIR_POTENTIALS_H__
