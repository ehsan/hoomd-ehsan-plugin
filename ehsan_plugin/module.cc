// Copyright (c) 2009-2019 The Regents of the University of Michigan

// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

// Include the defined classes that are to be exported to python
#include "ExampleUpdater.h"
#include "BoxResizeUpdater.h"
#include "AllEhsanPairPotentials.h"
#include "AllEhsanAnisoPairPotentials.h"
#include <hoomd/md/PotentialPair.h>
#include <hoomd/md/AnisoPotentialPair.h>
//#include "ContourForceCompute.h"

#ifdef ENABLE_CUDA
#include <hoomd/md/PotentialPairGPU.h>
#include <hoomd/md/AnisoPotentialPairGPU.h>
#include "ContourForceComputeGPU.h"
#endif

#include <hoomd/extern/pybind/include/pybind11/pybind11.h>

namespace py = pybind11;
//! Function to make the Gay-Berne parameter type
inline pair_ehsangb_params make_pair_ehsangb_params(Scalar epsilon, Scalar lperp, Scalar lpar)
    {
    pair_ehsangb_params retval;
    retval.epsilon = epsilon;
    retval.lperp = lperp;
    retval.lpar = lpar;
    return retval;
    }

//! Function to export the fourier parameter type to python
void export_pair_params(py::module& m)
{
    py::class_<pair_ehsangb_params>(m, "pair_ehsangb_params").def(py::init<>());
    m.def("make_pair_ehsangb_params", &make_pair_ehsangb_params);
}
// specify the python module. Note that the name must explicitly match the PROJECT() name provided in CMakeLists
// (with an underscore in front)
PYBIND11_MODULE(_ehsan_plugin, m)
    {
    export_ExampleUpdater(m);
    export_PotentialPair<PotentialPairEhsanLJ>(m, "PotentialPairEhsanLJ");
    export_AnisoPotentialPair<AnisoPotentialPairEhsanGB>(m, "AnisoPotentialPairEhsanGB");
    export_ContourForceCompute(m);
    export_EhsanBoxResizeUpdater(m);
    export_pair_params(m);

    #ifdef ENABLE_CUDA
    export_ExampleUpdaterGPU(m);
    export_PotentialPairGPU<PotentialPairEhsanLJGPU, PotentialPairEhsanLJ>(m, "PotentialPairEhsanLJGPU");
    export_AnisoPotentialPairGPU<AnisoPotentialPairEhsanGBGPU, AnisoPotentialPairEhsanGB>(m, "AnisoPotentialPairEhsanGBGPU");
    export_ContourForceComputeGPU(m);
    #endif

    }
